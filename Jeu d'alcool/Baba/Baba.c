#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int oCarte;

    while (1)
    {
        srand(time(NULL));
        const int MAX = 5, MIN = 1;
        oCarte = (rand() % (MAX - MIN + 1)) + MIN;

        switch (oCarte)
        {
        case 1:
            printf("Le Roi\n");
            break;
        case 2:
            printf("La Dame\n");
            break;
        case 3:
            printf("Le Vallet\n");
            break;
        case 4: 
            printf("Le 10\n");
            break;
        case 5:
            printf("Le 9\n");
            break;
        case 6:
            printf("Le 8\n");
            break;
        case 7:
            printf("Le 7\n");
            break;
        case 8:
            printf("Le 6\n");
            break;
        case 9:
            printf("Le 5\n");
            break;
        case 10:
            printf("Le 4\n");
            break;
        case 11:
            printf("Le 3\n");
            break;
        case 12:
            printf("Le 2\n");
            break;
        case 13:
            printf("L'as\n");
            break;
        default:
            printf("Error\n");
            printf("%d \n", oCarte);
            break;
        }
        system("pause");
        printf("\n");
    }
}