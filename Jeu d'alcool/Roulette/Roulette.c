#include <stdio.h>
#include <stdlib.h>

int main () {
   int i, n;
   time_t t;
   
   n = 5;
   printf("Chaque joueur choisit un chiffre entre 0 et 10.\nSi son chiffre sort il bois.\n");
   system("pause");
   do
   {
       /* Intializes random number generator */
        srand((unsigned) time(&t));

        /* Print 5 random numbers from 0 to 10 */
        for( i = 0 ; i < n ; i++ ) {
            printf("%d\n", rand() % 10);
        }
        system("pause");
   } while (n != 0);

   return(0);
}