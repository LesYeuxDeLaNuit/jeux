#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main (){
    int OMyst = 0, oInput = 0;
    const int MAX = 100, MIN = 1;
    srand(time(NULL));
    OMyst = (rand() % (MAX - MIN + 1)) + MIN;
    do
    {
        printf("Quel est le nombre ? ");
        scanf("%d", &oInput);
        if (OMyst > oInput)
            printf("C'est plus !\n\n");
        else if (OMyst < oInput)
            printf("C'est moins !\n\n");
        else
            printf ("Tu as trouve le nombre mystere !\n\n");
    } while (oInput != OMyst);
    system("pause");
    return 0;
}